Version XXX, released YYY:
  * Changes
    * Update to quick-xml 0.15
    * Add a default "comments" feature to transform comments into errors when unset.
Version 0.11.0, released 2019-06-14:
  * Breaking
    * Get rid of IntoElements, replace with `Into<Node>` and `<T: Into<Node> IntoIterator<Item = T>>`
  * Fixes
    * Remote unused `mut` attribute on variable
  * Changes
    * Update quick-xml to 0.14
    * Split Node into its own module
    * Nicer Debug implementation for NamespaceSet
Version 0.10.0, released 2018-10-21:
  * Changes
    * Update quick-xml to 0.13
    * Update doc to reflect switch from xml-rs to quick-xml.
Version 0.9.1, released 2018-05-29:
  * Fixes
    * Lumi fixed CDATA handling, minidom will not unescape CDATA bodies anymore.
  * Small changes
    - Link Mauve implemented IntoAttributeValue on std::net::IpAddr.
Version 0.9.0, released 2018-04-10:
  * Small changes
    - Upgrade quick_xml to 0.12.1
Version 0.8.0, released 2018-02-18:
  * Additions
    - Link Mauve replaced error\_chain with failure ( https://gitlab.com/lumi/minidom-rs/merge_requests/27 )
    - Yue Liu added support for writing comments and made the writing methods use quick-xml's EventWriter ( https://gitlab.com/lumi/minidom-rs/merge_requests/26 )
Version 0.6.2, released 2017-08-27:
  * Additions
    - Link Mauve added an implementation of IntoElements for all Into<Element> ( https://gitlab.com/lumi/minidom-rs/merge_requests/19 )
Version 0.6.1, released 2017-08-20:
  * Additions
    - Astro added Element::has_ns, which checks whether an element's namespace matches the passed argument. ( https://gitlab.com/lumi/minidom-rs/merge_requests/16 )
    - Link Mauve updated the quick-xml dependency to the latest version.
  * Fixes
    - Because break value is now stable, Link Mauve rewrote some code marked FIXME to use it.
Version 0.6.0, released 2017-08-13:
  * Big changes
    - Astro added proper support for namespace prefixes. ( https://gitlab.com/lumi/minidom-rs/merge_requests/14 )
  * Fixes
    - Astro fixed a regression that caused the writer not to escape its xml output properly. ( https://gitlab.com/lumi/minidom-rs/merge_requests/15 )
Version 0.5.0, released 2017-06-10:
  * Big changes
    - Eijebong made parsing a lot faster by switching the crate from xml-rs to quick_xml. ( https://gitlab.com/lumi/minidom-rs/merge_requests/11 )
